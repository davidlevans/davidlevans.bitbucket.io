import { TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  template: '<div>Spotting message!</div>',
  styleUrls: ['./colors.spec.scss']
})
class TestComponent {}

describe('Colour mixin function should', () => {
  let element: HTMLElement;
  let actuals: Map<string, string>;
  let expecteds: Map<string, string>;

  const retrieveMixinResults = () => {
    actuals = new Map();
    expecteds = new Map();
    for (let index = 0; ; index++) {
      const argument = retrieveMixinResult('argument', index);
      if (argument) {
        const expected = retrieveMixinResult('expected', index);
        const actual = retrieveMixinResult('actual', index);
        expecteds.set(argument, expected);
        actuals.set(argument, actual);
      } else {
        return;
      }
    }
  };

  const retrieveMixinResult = (mode: string, index: number) => {
    return getComputedStyle(element).getPropertyValue(`--test-${index}-${mode}`);
  };

  beforeEach(() => {
    const fixture = TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [TestComponent],
      providers: []
    }).createComponent(TestComponent);
    fixture.detectChanges();
    element = fixture.debugElement.query(By.css('div')).nativeElement;
    retrieveMixinResults();
  });

  it('be in accordance with cache', () => {
    actuals.forEach((value: string, key: string) => {
      expect(value).toBe(expecteds.get(key), 'upon color ' + key);
    });
  });
});
