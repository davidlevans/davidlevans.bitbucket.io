function is_touch_device() {
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    var mq = function(query) {
        return window.matchMedia(query).matches;
    }

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
        return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}

if (is_touch_device()) {
    //document.write("Your device is Touch");
    document.getElementById('klop').innerText = "Your device is touch"; 
    $('html').addClass('touch');
} else {
    //document.write("Your device is NOT touch");
   
    document.getElementById('klop').innerText = "Your device is NOT touch"; 
    $('html').removeClass('touch');
}


// $('.collapseable').on('show.collapse', function () {
//     $(this).siblings('.card-title').addClass('active');
//   });
 
//   $('.panel-collapse').on('hide.bs.collapse', function () {
//     $(this).siblings('.panel-heading').removeClass('active');
//   });